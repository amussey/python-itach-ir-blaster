import csv
import socket


class IRBlaster(object):
    commands = []
    command_mapping = {}

    def __init__(self, ip, port=4998, remote=None, csv_file=None):
        if not (remote or csv_file):
            raise Exception('Error: Either a built in remote model or an external csv file is required to create a new IRBlaster object.')

        self.ip = ip
        self.port = port

        csv_file_contents = open(csv_file, 'r').read()
        csv_file_contents = [x for x in csv_file_contents.splitlines() if x][1:]
        csv_lines = list(csv.reader(csv_file_contents))

        for line in csv_lines:
            self.command_mapping[line[0].lower()] = str.encode(line[1] + '\r')
            self.commands.append(line[0])

    def call(self, command, retry=True):
        command = command.lower()
        command_code = self.command_mapping.get(command, None)
        if command_code:
            try:
                self.socket.sendall(command_code)
            except Exception:
                if retry:
                    self._connect()
                    self.call(command, False)
                    return
                else:
                    raise Exception('There was a problem making the call!')
            else:
                response = self.socket.recv(128)  # This is the echo back from iTach
                print("Response: ", response)
        else:
            print('Command ', command, ' does not exist.')

    def _connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.ip, self.port))
