#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
A simple library for interfacing with iTach IR Blasters.

Copyright 2016 Andrew Mussey <git@amussey.com>
All rights reserved.
'''

from distutils.core import setup

doclines = __doc__.split("\n")

setup(
    name='itach-ir-blaster',
    version='0.1.0',
    description=doclines[0],
    long_description='\n'.join(doclines[2:]),
    keywords='ir blaster itach global cache infrared',
    maintainer='Andrew Mussey',
    maintainer_email='git@amussey.com',
    url='https://gitlab.com/amussey/python-itach-ir-blaster',
    platforms=['any'],
    install_requires=[''],
    packages=['ir_blaster'],
    scripts=[]
)
